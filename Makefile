COMP=gcc
LIB=asound

SRC=c
OBJ=o

NAME=Mompl
ROOT=$(CURDIR)
SRC_DIR=src
OBJ_DIR=obj
DBG_DIR=dbg
EXE_DIR=out

$OPT=

ALL: debug 

compile:
	$(foreach SRC, *.$(SRC), $(COMP) -c $(SRC_DIR)/$(SRC);)
	if [ ! -d $(OBJ_DIR) ]; then mkdir $(OBJ_DIR); fi
	mv *.$(OBJ) $(ROOT)/$(OBJ_DIR);

link:
	if [ ! -d $(EXE_DIR) ]; then mkdir $(EXE_DIR); fi
	$(COMP) $(OBJ_DIR)/*.$(OBJ) -o $(EXE_DIR)/$(NAME) -l$(LIB)

debug:
	if [ ! -d $(DBG_DIR) ]; then mkdir $(DBG_DIR); fi
	$(foreach SRC, *.$(SRC), $(COMP) -g $(SRC_DIR)/$(SRC) -o $(DBG_DIR)/Debug;)
